<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(
    [
        'middleware' => 'auth',//中间件
        'prefix' => 'admin',//在路由中增加前缀
        'namespace' => 'Admin',//命名空间 跟控制器里面的命名和文件夹有关系
    ], function () use ($router) {
    Route::get('/home', 'HomeController@index')->name('home');

});
Route::get('/test', 'HomeController@test');
Route::get('/tests', function () {
    return 'bigebao world';
});

