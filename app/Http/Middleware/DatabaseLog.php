<?php
/**
 * Created by PhpStorm.
 * User: lzw
 * Date: 12/6/17
 * Time: 5:01 PM
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DatabaseLog
{
    public function handle(Request $request, Closure $next)
    {
        if (env('APP_ENV') == 'local') {
            DB::connection()->enableQueryLog();
            $startTime = milliseconds();
            $response = $next($request);
            $endTime = milliseconds();
            Log::debug('response time: ' . ($endTime - $startTime) . 'ms');
            $logs = DB::getQueryLog();
            foreach ($logs as $log) {
                Log::debug($log['query']);
            }
            return $response;
        } else {
            return $next($request);
        }
    }
}
